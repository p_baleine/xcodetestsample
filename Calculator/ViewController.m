//
//  ViewController.m
//  Calculator
//
//  Created by 田嶋 隼平 on 2013/12/05.
//  Copyright (c) 2013年 田嶋 隼平. All rights reserved.
//

#import "ViewController.h"

#import "Calculator.h"

@interface ViewController ()
- (void)updateResultTextField;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onLhsEditingChanged:(id)sender {
    [self updateResultTextField];
}
- (IBAction)onRhsEditingChanged:(id)sender {
    [self updateResultTextField];
}

- (void)updateResultTextField {
    int lhs = [self.lhsTextFiels.text intValue];
    int rhs = [self.rhsTextField.text intValue];
    NSString *result = [NSString stringWithFormat:@"%d",
                        [Calculator plus:lhs and:rhs]];
    self.resultTextField.text = result;
}
@end
