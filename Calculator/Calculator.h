//
//  Calculator.h
//  Calculator
//
//  Created by 田嶋 隼平 on 2013/12/06.
//  Copyright (c) 2013年 田嶋 隼平. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calculator : NSObject
+ (int)plus:(int)lhs and:(int)rhs;
@end
