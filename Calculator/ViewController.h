//
//  ViewController.h
//  Calculator
//
//  Created by 田嶋 隼平 on 2013/12/05.
//  Copyright (c) 2013年 田嶋 隼平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *resultTextField;
@property (weak, nonatomic) IBOutlet UITextField *rhsTextField;
@property (weak, nonatomic) IBOutlet UITextField *lhsTextFiels;
- (IBAction)onRhsEditingChanged:(id)sender;
- (IBAction)onLhsEditingChanged:(id)sender;
@end
