//
//  AppDelegate.h
//  Calculator
//
//  Created by 田嶋 隼平 on 2013/12/05.
//  Copyright (c) 2013年 田嶋 隼平. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
