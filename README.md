# ユニットテストとUIテストのサンプルアプリ

## テストの実行

### ユニットテスト

![Xcode](https://bitbucket.org/p_baleine/xcodetestsample/raw/5bac586209f867da95ebd6546dc1d6abd5bdcf1b/images/xcode.png)

テスト名の辺りにマウスを持って行くと三角マークが表示されるのでこれをクリックする

### UIテスト

1 Xcodeのメニュー Product > Profile で出てくるダイアログでAutomationを選択してProfileをクリックするとInstrumentsが開く

2 Instrumentsの左側のScriptsのAdd > Importからtest.jsをインポートする。

3 スクリプトを表示して一番下の再生マークをクリックする
