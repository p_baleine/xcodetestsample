//
//  CalculatorTests.m
//  CalculatorTests
//
//  Created by 田嶋 隼平 on 2013/12/05.
//  Copyright (c) 2013年 田嶋 隼平. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "Calculator.h"

@interface CalculatorTests : XCTestCase

@end

@implementation CalculatorTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPlus
{
    XCTAssertTrue([Calculator plus:1 and:1] == 2);
}

@end
