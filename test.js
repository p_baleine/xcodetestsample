
var target = UIATarget.localTarget();
var app = target.frontMostApp();
var window = app.mainWindow();

var TEST_NAME = "足し算のテスト";

// 要素ツリーを見てみる
target.logElementTree();

var lhsInput = window.textFields()[0];
var rhsInput = window.textFields()[1];

// 値を入れて、
lhsInput.setValue(123);
rhsInput.setValue(456);

// ちょっとまってみて、
target.delay(2);

// assertしてみる
var resultInput = window.textFields()[2];
var resultValue = resultInput.value();

if (resultValue === "579")
	UIALogger.logPass(TEST_NAME);
else
	UIALogger.logFail(TEST_NAME);
